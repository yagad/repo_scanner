#!/bin/bash
x=0
while  read i; 
do 
 let "x=x+1"
 echo "$x. $i"
 python count_files.py --dir=stage/$i --resultDir=results --lang=ccpp
 wait
done<targets 

python aggregate_results.py --dir=results --resultDir=results
