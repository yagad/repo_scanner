import yagad_utils
import getopt
import os 
import sys
import pandas as df
import numpy as np
ALLOWED=3
shortopts=['h']
longopts=['lang=','dir=','resultDir=']
LANGUAGES=set(['c','cpp','ccpp'])
EXTENSIONS={}
KEYWORDS={}
logger=None


FILE_TYPE="file_type"
STRING_MATCH='string_match'

class Settings:
	def __init__(self,language='c',keywords=[],source_dir='.'):
		self.language=language
		self.keywords=keywords
		self.source_dir=source_dir
                self.resultDir=source_dir
class Results:
	def __init__(self,name):
		self.data={}
                self.name=name
		self.categories=0
		self.inventory={}

	def add_result(self,category,key,data):
		if category not in self.data.keys():
			self.__add_category(category)
                if key not in self.data[category]:
                    self.data[category][key]={}
                    self.inventory[category]+=1
                if type(data) is dict:
                    self.data[category][key]=data
                else:
                    self.data[category][key]={'Value':data}

	def __add_category(self,category):
			self.data[category]={}
			self.categories+=1
			self.inventory[category]=0

	def print_csv(self,category):
		category_data=self.data[category]
                csv=''
		for key in category_data:
                        values=category_data[key].values()
                        if type(values) is not  list:
                            values=[values]
			csv+='{},{},{}\n'.format(category,key,','.join([str(i) for i in values]))
		return csv


        def __str__(self):
            msg='package,{}\n'.format(self.name)
            for category in self.data:
                msg+='{}' .format(self.print_csv(category))
            return msg

def init():
	global EXTENSIONS
	global KEYWORDS
	global settings
        global logger

	EXTENSIONS['c']=['.c','.h','.S','.o','.s']
	EXTENSIONS['cpp']=['.cpp','.hpp','.S','.o','.s']
	EXTENSIONS['ccpp']=list(set(EXTENSIONS['c']+EXTENSIONS['cpp']))
	KEYWORDS['c']=['dlopen','dlsym']
	KEYWORDS['cpp']=['dlopen','dlsym']
	KEYWORDS['ccpp']=['dlopen','dlsym']
	settings= Settings(language='c',keywords=KEYWORDS['c'])
        logger=open('reposcan.log','w+')

def parse_args():
        global settings
	try:
		opts,args = getopt.getopt(sys.argv[1:],shortopts,longopts)
	except getopt.GetoptError as err:
		print err
		sys.exit(2)
	for o,a  in opts:
		if o == '--lang':
			lang=a
			message('Setting language {}'.format(lang))
			assert lang in LANGUAGES,'Not supported {}'.format(lang)
			message('Has {}  files and will look for :  {} keywords'.format(EXTENSIONS[lang],KEYWORDS[lang]))
		elif o == '--dir':
                    settings.source_dir=a
                elif o == '--resultDir':
                    settings.resultDir=a
                else:
                    assert False,'Argument not Supported {}'.format(o)

def LOG(msg):
    global logger
    logger.write(msg)
    logger.write('\n')

def message(msg,lvl=3):
	global ALLOWED
	if lvl<=ALLOWED:
		print (msg)

def check_infile(f,_text):
    found=False
    matches={}
    if type(_text) is not list:
        keywords=[_text]
    else:
        keywords=_text
    for text in keywords:
        matches[text]=0
    with open(f,'r') as target:
        for n,line in enumerate(target.readlines()):
            for text in keywords:
                if text in line:
                    if found==False:
                        found=True
                        LOG('Found match in {}'.format(f))
                    LOG ('{}. {}'.format(n,line))
                    matches[text]+=1
    return matches


def update_matches(matches,new_matches):
    for key in new_matches:
        if key not in matches:
            matches[key]=0
        matches[key]+=new_matches[key]


def calculate_metacolumns(full_scope):
        full_scope['isC']=False
        if '.c' in full_scope.columns:
            full_scope['isC'].loc[full_scope['.c']>0]=True
        full_scope['isC++']=False
        if '.cpp' in full_scope.columns:
            full_scope['isC++'].loc[full_scope['.cpp']>0]=True

        full_scope['hasAssembly']=False
        if '.s' in full_scope.columns or '.S' in full_scope.columns:
            full_scope['hasAssembly'].loc[full_scope['.s']>0]=True
            full_scope['hasAssembly'].loc[full_scope['.S']>0]=True

        full_scope['assemblyInLine']=False
        if '__asm__' in full_scope.columns :
            full_scope['assemblyInLine'].loc[full_scope['__asm__']>0]=True

        full_scope['hasDynamicLoading']=False
        if 'dlopen' in full_scope.columns :
            full_scope['hasDynamicLoading'].loc[full_scope['dlopen']>0]=True

        return full_scope

def generate_empty(keys):
    row={}
    for key in keys:
        row[key]=0
    return row

def main():
	global EXTENSIONS
	global KEYWORDS
	global settings
	parse_args()
        full_scope=df.DataFrame(columns=['name','category','isC','isC++','hasAssembly','assemblyInLine','hasDynamicLoading'])
        files=[]
        yagad_utils.recursiveFileFind(settings.source_dir,'.csv',returnResults=files)
        for f in files:
            table=df.read_csv(f)
            if 'key' not in table.columns:
                print "error no  key in {}".format(f)
                continue
            keys=table['key'].unique().tolist()
            for key in keys :
                if key not in full_scope.columns:
                    full_scope[key] = df.Series(np.zeros(len(full_scope)))
            
            new_row=generate_empty(full_scope.columns)
            for i,row in table.iterrows():
                try:
                    new_row[row.key]=int(row.value)
                except Exception:
                    try:
                        new_row[row.key]=str(row.value)
                    except Exception:
                        new_row[row.key]=-1


#            if len(full_scope)==20:
#                sys.exit(2)
            full_scope=full_scope.append(new_row,ignore_index=True)
        
        full_scope=calculate_metacolumns(full_scope)
        message('{}\n'.format(full_scope))
        fname='{}/{}.csv'.format(settings.resultDir,"{}_files_aggregate.csv".format(len(full_scope)))
        message('Saving results to {}'.format(fname))
        full_scope.to_csv(fname)
#        with open(fname,'w') as out:
#            out.write(str(results))





if __name__ == '__main__':
    init()
    main()


