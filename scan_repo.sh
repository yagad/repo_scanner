#!/bin/bash 

#package=/var/lib/apt/lists/gr.archive.ubuntu.com_ubuntu_dists_bionic-updates_main_binary-amd64_Packages 
#package=/var/lib/apt/lists/gr.archive.ubuntu.com_ubuntu_dists_bionic_main_binary-amd64_Packages
#package=/var/lib/apt/lists/security.ubuntu.com_ubuntu_dists_bionic-security_main_binary-amd64_Packages

package=/var/lib/apt/lists/gr.archive.ubuntu.com_ubuntu_dists_bionic_universe_binary-amd64_Packages

packages=$(cat $package | grep ^Package | cut -d':' -f2 | tr  '\n\r' ' ')
pname=`basename $package`
stage=stage
results=results
result_file=$results/$pname
VERBOSE=true
target=$stage/dummy

echo "$packages">.tmp

function TIMESTAMP {
   start_time=`date`
   echo "$start_time"
}




function MESSAGE {
msg=$1

if [ "$VERBOSE" = true ];  then 
	echo "${msg}" >>.log
fi


}


function download {
	package=$1
	#mkdir $stage/$package
        target=$stage/$package
	MESSAGE "Downloading $target"
	cd $stage/
	echo "-->$target, $package"
	wait
	apt-get source $package
	wait 
	retval=$target
	cd -

}

function proccess {
	 target=$1
	 MESSAGE "Processing $target"
	 retval="${target},is,off" #place result here

}

function add_result {
	 result=$1
	 MESSAGE "Results *$result*   $target"
	 retval="$result" >> $result_file
}
function clean {
	target=$1
	MESSAGE "removing $target"
	cd $stage
	wait
	find . -name "*.gz" -type f -delete
	find . -name "*.dsc" -type f -delete
	find . -name "*.xz" -type f -delete
	#rm -r $target
	wait
	cd -
}

t=$(TIMESTAMP)
echo "***Start,$t" > $result_file
x=0
for p in ${packages[@]};

do 
 let "x=x+1"
 if [[ "$x" -gt 51800 ]]; then 
	 MESSAGE "$x.Checking  $p"
	 wait
	 target=$(download $p)
	 wait
	 res=$(proccess $target)
	 add_result $res
	 wait
	 if [[ "$x" -gt 54951 ]]; then 
		 break
	 fi
 fi
done 

clean "all"
t=$(TIMESTAMP)
add_result "Proccessed $x Packages from $pname"
MESSAGE  "***End,$t" 
