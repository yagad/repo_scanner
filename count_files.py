import yagad_utils
import getopt
import os 
import sys


ALLOWED=3
shortopts=['h']
longopts=['lang=','dir=','resultDir=']
LANGUAGES=set(['c','cpp','ccpp'])
EXTENSIONS={}
KEYWORDS={}
logger=None


class Settings:
	def __init__(self,language='c',keywords=[],source_dir='.'):
		self.language=language
		self.keywords=keywords
		self.source_dir=source_dir
class Results:
	def __init__(self,name):
		self.data={}
                self.name=name
		self.categories=0
		self.inventory={}

	def add_result(self,category,key,data):
		if category not in self.data.keys():
			self.__add_category(category)
                if key not in self.data[category]:
                    self.data[category][key]={}
                    self.inventory[category]+=1
                if type(data) is dict:
                    self.data[category][key]=data
                else:
                    self.data[category][key]={'Value':data}

	def __add_category(self,category):
			self.data[category]={}
			self.categories+=1
			self.inventory[category]=0

	def print_csv(self,category):
		category_data=self.data[category]
                csv='category,key,value\n'
                csv+='package,name,{}\n'.format(self.name)
		for key in category_data:
                        values=category_data[key].values()
                        if type(values) is not  list:
                            values=[values]
			csv+='{},{},{}\n'.format(category,key,','.join([str(i) for i in values]))
		return csv


        def __str__(self):
            msg=''
            for category in self.data:
                msg+='{}'.format(self.print_csv(category))
            return msg

def init():
	global EXTENSIONS
	global KEYWORDS
	global settings
        global logger

	EXTENSIONS['c']=['.c','.h','.S','.o','.s']
	EXTENSIONS['cpp']=['.cpp','.hpp','.S','.o','.s']
	EXTENSIONS['ccpp']=list(set(EXTENSIONS['c']+EXTENSIONS['cpp']))
	KEYWORDS['c']=['dlopen','dlsym']
	KEYWORDS['cpp']=['dlopen','dlsym']
	KEYWORDS['ccpp']=['dlopen','dlsym']
        
        
        logger=open('reposcan.log','w+')

def parse_args():
        global settings
	settings= Settings(language='c',keywords=KEYWORDS['c'])
	try:
		opts,args = getopt.getopt(sys.argv[1:],shortopts,longopts)
	except getopt.GetoptError as err:
		print err
		sys.exit(2)
	for o,a  in opts:
		if o == '--lang':
			settings.language=a
			message('Setting language {}'.format(settings.language),lvl=4)
			assert settings.language in LANGUAGES,'Not supported {}'.format(settings.language)
			settings.keywords=KEYWORDS[settings.language]
			message('Has {}  files and will look for :  {} keywords'.format(EXTENSIONS[settings.language],settings.keywords),lvl=4)
		elif o == '--dir':
                    settings.source_dir=a
                elif o == '--resultDir':
                    settings.resultDir=a
                else:
                    assert False,'Argument not Supported {}'.format(o)

def LOG(msg):
    global logger
    logger.write(msg)
    logger.write('\n')

def message(msg,lvl=3):
	global ALLOWED
	if lvl<=ALLOWED:
		print (msg)

def check_infile(f,_text):
    found=False
    matches={}
    if type(_text) is not list:
        keywords=[_text]
    else:
        keywords=_text
    for text in keywords:
        matches[text]=0

    with open(f,'r') as target:
        for n,line in enumerate(target.readlines()):
            for text in keywords:
                if text in line:
                    if found==False:
                        found=True
                        LOG('Found match in {}'.format(f))
                    LOG ('{}. {}'.format(n,line))
                    matches[text]+=1
    return matches


def update_matches(matches,new_matches):
    for key in new_matches:
        if key not in matches:
            matches[key]=0
        matches[key]+=new_matches[key]

def main():
	global EXTENSIONS
	global KEYWORDS
	global settings
        extensions_roi=['.c','.cpp','.h','.hpp']
        text=KEYWORDS[settings.language]
        text.append('__asm__')
        matches={}
        results=Results(os.path.basename(settings.source_dir))
	for extension in EXTENSIONS[settings.language]:
                files=[]
		message('Checking for {}'.format(extension))
		yagad_utils.recursiveFileFind(settings.source_dir,extension,returnResults=files)
                count=len(files)
                results.add_result('file_type',extension,count)
                if count>0:
                    if extension in extensions_roi:
                        for f in files:
                            new_matches=check_infile(f,text)
                            update_matches(matches,new_matches)

		message('Found {} files'.format(count))
        message('Found {} text inside {} files ({})'.format(text,matches,extensions_roi))
        for text in matches:
            results.add_result('string_match',text,matches[text])

        message('{}\n'.format(results))
        fname='{}/{}.csv'.format(settings.resultDir,results.name)
        message('Saving results to {}'.format(fname))
    
        with open(fname,'w') as out:
            out.write(str(results))





if __name__ == '__main__':
    init()
    parse_args()
    main()


